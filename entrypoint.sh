#!/bin/sh

# This is the production entrypoint.
# DO NOT USE IN DEVELOPMENT.

python3 manage.py makemigrations
python3 manage.py migrate
#python3 manage.py compilemessages -l fr
python3 manage.py collectstatic --no-input

gunicorn PinList.wsgi -b unix:/run/pinlist/unix.sock