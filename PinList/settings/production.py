from PinList.settings import *

if 'SECRET_KEY' in os.environ:
    SECRET_KEY = os.environ['SECRET_KEY']
else:
    raise ImproperlyConfigured('SECRET_KEY was not found. It is fetched from system as an environment variable.')

DEBUG = False

ALLOWED_HOSTS = ['*']
