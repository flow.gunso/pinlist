from django.shortcuts import render
from PinList.settings import PINBOARD_API_TOKEN, PINBOARD_TAG
import pinboard


def index(request, filtered_tag=None):

    # Initialize link with Pinboard.
    pb = pinboard.Pinboard(PINBOARD_API_TOKEN)

    # Load posts from either Pinboard or current user session.
    if 'posts' not in request.session:
        posts = pb.posts.all(tag=[PINBOARD_TAG])
        request.session['posts'] = posts
        request.session['update'] = pb.posts.update()
    else:
        posts = request.session['posts']
        if pb.posts.update() > request.session['update']:
            print("Pinboard has changed, fetching posts from Pinboard")
            posts = pb.posts.all(tag=[PINBOARD_TAG])
            request.session['posts'] = posts
            request.session['update'] = pb.posts.update()

    # Filter posts when a filtered tag is requested.
    if filtered_tag is not None:
        tmp = []
        for post in posts:
            if filtered_tag in post.tags:
                tmp.append(post)
        posts = tmp

    return render(request, "index.html", {
        'posts': posts,
        'filtered_tag': filtered_tag,
    })