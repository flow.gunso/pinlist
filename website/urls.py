from django.urls import path
from website.views import index

urlpatterns = [
    path('', index, name="index"),
    path('tag/<str:filtered_tag>', index, name="filtered_index")
]