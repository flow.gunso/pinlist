# CHANGELOG

# [1.0] - 2018/12/02
- Minor tweaks to designs:
  - Margins to the filtering tag and to the footer
- Merge CSS designs
- Add a source link to this repository
- Add FontAwesome
- Create a logo, a push pin with multiple pins
- Prepare documentation

# [@0ad63f4196f70a75f3367961493f4b7af4124bab] - 2018/12/01
- First staging release