[![logo](docs/logo.png)](a_website_may_be_next)
# PinList

[![pipeline status](https://gitlab.com/flow.gunso/pinlist/badges/master/pipeline.svg)](https://gitlab.com/flow.gunso/pinlist/commits/master)

PinList is a web application that fetch specificly tagged bookmarks from a Pinboard account, then display them.

![Index](docs/demo_index.png) | ![Index with a filtering tag](docs/demo_responsive-filtered-index.png) 