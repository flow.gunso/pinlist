FROM python:3.6-alpine

COPY . /app
WORKDIR /app/

RUN pip3 install -r requirements.txt

ENV PYTHONUNBUFFERED=1
ENV DJANGO_SETTINGS_MODULE=PinList.settings.production

ENTRYPOINT ["/bin/sh", "entrypoint.sh"]

